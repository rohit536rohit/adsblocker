import asyncio
import requests
from flask import Flask, request
import pyrogram

app = Flask(__name__)

@app.route("/webhook", methods=["GET","POST"])
def get_token():
    # Get the bot token from the API
    data = request.get_json()
    print(data)
    bot_token = request.args.get("token")
    requests.post("https://api.bots.business/tg_webhooks/"+bot_token, json=data)
    asyncio.run(run_client(bot_token))
    return "got it", 200

async def run_client(bot_token):
    app = pyrogram.Client(session_name="my_client")
    await app.start(bot_token=bot_token)

    @app.on_message()
    async def handler(client, message):
        if '#AD' in message.text or '#paidAD' in message.text or 'bots.business/ads' in message.text or '#PaidAd' in message.text or 'sponsored' in message.text:
            await message.delete()
            print('Message deleted successfully')
        else:
            print('Message not deleted')
            pass

    await app.idle()
    await app.stop()

if __name__ == "__main__":
    app.run()
