import asyncio
import requests
from flask import Flask, request
import pyrogram

app = Flask(__name__)

@app.route("/webhook", methods=["GET","POST"])
def handle_webhook():
    try:
        data = request.get_json()
        print(data)
        bot_token = request.args.get("token")
        requests.post("https://api.bots.business/tg_webhooks/"+bot_token, json=data)
        async def run_and_idle(bot):
            async with bot:
                current_message_id = data['message']['message_id']
                chat_id = data['message']['chat']['id']
                messages = await bot.get_messages(chat_id=chat_id, ids=[current_message_id + i for i in range(1, 6)])
                for message in messages:
                    if '#Ad' in message.text or '#AD' in message.text or '#paidAD' in message.text or '#paidad' in message.text or 'bots.business/ads' in message.text or '#PaidAd' in message.text or 'sponsored' in message.text:
                        await message.delete()
                        print('Message deleted successfully')
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        bot = pyrogram.Client(name="my_client"+bot_token,bot_token=bot_token,   api_id=17484848,api_hash="4d6acdeff347f8103a71c18b11e751bc")
        loop.run_until_complete(run_and_idle(bot))
        close_client(bot)
        return "got it", 200
    except Exception as e:
        print(e)
        return "Hi", 200

async def close_client(bot):
    await bot.stop()


if __name__ == "__main__":
    app.run()
